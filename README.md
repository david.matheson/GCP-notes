platform provides
-infra services for creating vms
- services that make deployment easy
- allow you to securaley store data
-big dat aservices allow you to manage it in ral-time
-also ability to do machine learning in real time


# Introducing to Google Cloud Platform
* enables devs to build-, test, and deploy apps on Google's infrastructure

Googles infrastructure
* Data centers - lots of em for computation and backend storage
* has a 'backbone network' to interconnect data centers and to deliver traffic to the 'Edge points of presence' (POP)
* Points of presence - 70+ edge point of presence in 33 countries
* edge caching - theres an edge caching platform on top of their infrastructure...data's cached close to users for better performance and cost savings. when content is delivered from the cache, theres no charge. 

Data centers are located in specific geographical locations called 'regions', further broken down by 'zones'. 

Pricing
* google does sub-hour billing...pay by the minuite instead of by the hour..
* also 'sustained use discounts' for folks that run over 25% of a calendar month
* compute engine custom machine types - pay only for the resources tyou need for your app


kubernetes...google's container orchestration system...

### cloud computing history
1. colocation
2. virtualized data centers - where we are now
3. global elastic cloud - services are fully managed by the cloud provider and customers focus on building apps



* deployment manager - lets you view and manage your cloud launcher deployments.


we deploy a LAMP stack server via Google Cloud Platform on `http://35.196.179.120/`

if we wanna log-in, we can do so via `ssh`...this is just a bash shell terminal provided by our linux VM thats been configured with Lamp Stack...so we can type in linux commands to configure our machine or deploy our software, etc.

steps:
1. console.cloud.google.com
2. login with gmail
3. create an account with a free trial + $300 to learn the platform
4. we create a project (LAMP Stack)

# Getting started with Google Cloud Platform

what is a google cloud platform project?
* think of it as a container for billing, permissions, and resources.
* each project you create is attached to a billing account
* once you have a project, you can start using GCP Resources (like LAMP Stack)

* Projects are identified in 3 ways: a name, a project number, a project ID (a string).


### IAM
* Identity and access management - who can do what on which resource...permissions basically.

 list of permissions:
 ```js
compute.instances.delete
compute.instances.get
compute.instances.list
compute.instances.setMachineType
compute.instances.start
compute.instances.stop
 ```

 ### Service accounts 
 * provide an identity for carrying out `server-to-server` interactions in a project
 * are used to authenticate from one server to another
 * are identified with an email address..ex: `<project_number>@developer.gserviceaccount.com`
* service accounts has a key instead of a password

# Interacting with Google Cloud Platform
3 common ways of managing resources with google cloud platform

* web UI - 
* google cloud SDK from the command line - automate administrative tasks...cloud SDK can be installed on local machines, this allows you to write scripts to make this automation happen.
* we can also programmatically control GCP resources using a REST api


### Google Cloud Platform console 
* browser-based tool for projects.
* can be used to maange project resources, and also used as a dshboard for monitoring resources and applications, viewing logs and more. many developer tools are available here. 

Cloud source repos - provide git repos
Google Cloud shell - a CLI in the browser, so you dont have to DL the SDK on your system...its always available for you.


Google Cloud SDK
* includes CLI tools for cloud platform products and services:
    * gcloud - main CLI for GCP products/services
    * GSutil - cloud storage

* Google Cloud shell - provides a linux VM instance running with the cloud SDK already installed. You can use it for CL access via a terminal window provided by your browser.

^ Available so you can do CLI stuff without having an envionment on your own computer. 


### RESTful APIs
* provide access to the platform
* these expose consistent interfaces using standard HTTP requests with json data provided from view-server intercharge
* OAuth 2.0 for authentication and authorization
* Most APIs included daily quotas

* you can use APIs Explorer to checkout all available APIs. 


**In addition to APIs,Client Libraries are also avaialble for accessing GCP for many of the most used programming languages like python and node.js**

### Client Libraries
* find a library that will work with your fav. language.
* google apis client libraries



# Google app engine and cloud datastore
* app engine = googles PAAS offering, makes deploying apps easy
* cloud datastore = completely managed noSql database that can be used with 'app engine' apps

# app engine
- PAAS for building scalable web apps and mobile backends..
- makes deployment, maintainence and scalability easy so you can focus on innocation.

 write your apps and deploy with one command...app engine takes care of the details.
 ex: 
 * bug fix, app engine runs the new code instead of the old code upon fixing.
 * app is busy? app engine will create new instances for you, and whenever its not, it'll shut those instances down.

 case study: snapchat
 * app engine scaled during growth to millions of users so now a small team is able to innovate quickly and expand globally.
 * app engine also allowed pushing out of features easily and versions of the app...super difficult if your app is constantly in use of millions of ppl

 ## 2 environments:
 ### Standard Environment
 * autoscale workloads to meet demands....so itll add/remove instances on demand..itll even scale to 0 if theres noone there
 * free daily quote, usage-based pricing...which is nice 

* SDKs for development, testing, and deployment
* standard workflow
    1. develop and test app locally
    2. use the SDK to deploy to App Engine
    3. App engines automatically scales and reliably serves your web app...creating new instances where needed. App Engine can also access a variety of services using dedicated APIs for things like Logs, Search, Task queues, and so forth.

**QUESTION: why need another instance? what is an instance?**

- memcache...allows you to cache data in memory when the same data is asked for multiple times.



### Flexible Environment
* when you need more flexbility than standard.
* uses docker containers (standard uses containers that are specific to google)
* google provides pre-configured docker containers for this...you can create your own docker containers though too...for example, if you wanted to create a custom container in asp.core bc you like c# a lot...so you can write your code in your fav language but deploy it easily to app engine, which takes care of all the infrastructure scaling, health monitoring, etc for you.

* deployment a lil diff from standard environment...it runs docker containers on compute engine VMs, you're charged just on the VMs your app is running on. bc the env uses docker, you can also run your apps on your dev machine...just install docker on your machine.

* you can access the same services as Standard envs can.

## Google Cloud Endpoints
* allow you to create RESTful APis for your services that look like the apis google creates for their services.
* in addition to making service interfaces easier, cloud endpoints also make it easier to integrate OAUTH2 into your own services...also helps your services integrate with other apps, using the same tools that are used to call Google services. 

## Google Cloud Datastore
* noSql db for your apps
* just one of several services you can use to store your data in GCP (also cloud SQL, Cloud Storage, and Cloud Bigtable)

* what is it? Its a NoSQL stored managed by google...schemaless: you store entities and entities have properties and properties have values.

you store an entity anyway you like..later on you can change it without worrying about conforming to a DB schema. 

* also since its schemaless you can store the same kind of entity in diff ways for diff instances!
* you can also use devtools to simulate Datastore on your local machine
* Datastore is a 'no-ops service' meaning you dont have to configure it in any way, it'll scale automatically as your app grows, stores your data across multiple zones for high availablility and fault tolerance.

* also supports ACID transactions: a transactions allows you to update multiple entities as a group so if any operation fails, all of them will fail so your data stays in a consistent state.
* Datastore also integrates seamlessly with App Engine

## Lab
- deploy a python app to the App Engine standard runtime environment
- Test the app and inspect data saved to Cloud Datastore

So how does this work?

you have the user's computer/phone, it goes to the App (via HTTP) hosted on App Engine, which is connected to App Engine Datastore for NoSQL Storage.

* actual lab...
in GCP we go to our existing project, create a repo, use Google Cloud Shell to create a folder, cd into it, clone another repo(???) called 'default', cd into that, pull some code for a 'bookshelf' app into it from Github, push it to the master branch of our initial GCP repo.

we can now look at our code;
- `app engine` - hosts the source code for the app to deploy in the lab
- `cloud-storage` - hosts the source code for a varaition of the app you deploy in the lab that includes support for saving data to cloud storage.
- `compute-engine` - contains the source coe for a version of the bookshelf app that runs on 'compute engine'
- `container-engine` - contains source code for version of app that runs on 'container engine'

^ question: why do they all run on a different engine?? 

`app-engine/app.yaml` - contains Python app's runtime configuration
`app-engine/main.py` - this file imports the bookshelf code and loads any relevant configuration data into the app....also includes osme code used to allow the app to run locally using the dev server included with App Engine SDK.
`app-engine/config.py` - contains all config values for the app
`app-engine/app.yaml` - contains Python app's runtime configuration

we go into `/bookshelf` and examine whats going on...`crud.py` acts as the controller layer for CRUD operations. lines 22-32:

```python
# [START list]
@crud.route("/")
def list():
    token = request.args.get('page_token', None)
    books, next_page_token = get_model().list(cursor=token)

    return render_template(
        "list.html",
        books=books,
        next_page_token=next_page_token)
# [END list]
```
^ see how theres no direct reference to the type of storage being used? we abstract away the specific operation required to read/write from the storage system and passed data data b/w the view and the storage model. This is advantageous because we can easily substitute Cloud Datastore for another storage service like Cloud SQL during development.

next steps: we go into `app-engine`, use `pip` to install python packages from `requirements.txt`. once we install these libraries, they're loaded in `/lib`.

Now since we've created our own gCloud repo, grabbed code from github, and installed the dependencies...we're ready to deploy. 

```
gcloud app deploy
```

Next steps: Cloud Datastore Overview:
* A `kind` in Cloud Datastore is kinda like a table in a relational DB. `Kinds` are used to categorize entities for queries.
* Data objects in Google Cloud Datastore are known as `entities`. An `entity` has one of more named properties...each property can have one or more values. Entities are identified by a unique key and are akin to rows in a relational DB. 

**These unique characteristics imply a diff. way of designing/managing data to take advantage of the ability to scale automatically.**

* Cloud Datastore also organizes data into 'entity groups'....this consists of a root entity and all of its descendants. Apps typically use entity groups to organize highly related data to achieve strong consistency and to enfore 'transactionality'.
EX: an app could use an entity gorup to store data about 1 product, or 1 user profile.


[[[KIND/ENTITY/PROPERTY IMAGE HERE]]]



### Review
* App Engine = managed computing environment that lets you focus on writing and running code. You dont need to create,configure, or manage any servers, storage, or networks to get starts. App Engine handles all of the web traffic generated by your users and automatically scales to meet changes in traffic levels.

* Google Cloud Datastore = NoSQL document DB, compliments app engine via automatic scaling, high performance, ease of app development.



# Google Cloud Platform fundamentals
many storage options in google cloud...lets look at them

### Google Cloud Storage
* allows you to use Google to store your data...fast and vasically unlimited in space.
* immutable BLOB storage = aka you can store anything: text,files, video, etc
* its NOT a file system, but it can be accessed as one (like git) using 3rd party tools
* data is automatically encrypted at rest and in flight...happens out the box. 
* google cloud storage stores and replicates your data across multiple devices and multiple zones to ensure durability.
* theres a number of different classes of storage....each class designed for a diff use case:
    * first step to using cloud storage is to create a 'bucket', you can then upload data into that bucket...you choose the region(s) you want that bucket to be in, and you choose the class of storage of the bucket...theres a number of them:
        - standard - regional or multi-regional (99.9% availability around the world..ex: gaming/mobile apps)..use regional storage when storing frequently accessed data in the same region as your google cloud compute instance that uses it, like Data Analytics
        
        
        - DRA (coldline) storage
        - Nearline - storage classes used for Data that's infrequently accessed: backups, archives, or data thats intended for disaster recovery. 

        ^ these 2 options have a much lower cost than standard storage. 


Autism Speaks = wanted to sequence the whole genomes of 1000s of individuals but didnt have the storage capacity to do this. Google provided the storage and the system capable of processing these massive datasets so now the org can securely store these complex datasets...nearly 100 TB of data so far. 



- Use regional buckets if your data is only being used in one region, otherwise use multi-regional.

#### Other Cloud Storage Features
* buckets support object versioning...so you can roll back to older versions as needed. 
* offline imports - allow you to copy large amounts of data onot physical devices and have it loaded into storage for you...supported by 3rd parties
* flexible control over who can access buckets and objects - by default objects are accessible only by their owners....you can allow permissions (read/write)
* you can set up object management life cycle rules - ex: if a version of an object more then 30 days old, move it automatically to cold storage. 
* change notifications - an event that notifies a program that something has happened to your data...ex: notify an image processing service every time an image is uploaded to a cloud storage bucket.

**cloud storage is often the ingestion point for data being moved into the cloud and is frequently the long-term storage location for data.**

# Google Cloud BigTable
A no-ops, NoSQL DB service for large-worldload apps (terrabytes to petabytes)

* native compatibility with Hadoop for big data stuffs.
* drives major apps like Google Analytics and Gmail
* typically customers use bigtable when they need a large amount of structured storage (bigger than a terrabyte)

EX: Sungard needed a repo of all equities & options quotes, orders, and events...theres billions of events/hour...many petabytes over years...BigTable handles data at this volume and velocity pretty well. 


#### How do you access BigTable? 
reading and writing from bigtable can happen via:
1. app API that can be written into your own application: ex: you could write a service in java to collect data and write it into bigTable OR you could build a dashboard app to provide data analysis on bigTable data

2. data can be streamed real-time into BigTable...stream processing tools like Spark Streaming and Storm allow this.

3. you can also do Batch Processing 

# Google Cloud SQL
This is google-managed MySQL relation DB in the cloud, this uses pre-configured VM to run mySQL DBs. typical tasks are completely done for you: patch mgmt is done automatically, back-ups are done automatically, etc.

* when you create a Cloud SQL DB, you specify how much space you need, CPU and RAM your DB server needs and you only pay for the resources you allocate. 

* you can create these with Google Cloud Console or CL tools. 
* you can control cloudSQL programmatically using REST APIs
* this is ideal when you need to store relational Data in the cloud 

* when stored, your info is encrypted.
* every network instance comes with its own firewall allowing you to control network access to your DB instance by grandting access. 

**Cloud SQL is just MySQL, so if you already know MySQL or a relational DB system, its easy to use.**

### Comparing Storage Options
which to use when?
#### BLOB
* cloud storage = good for any type of binary object...this is ideal and affordable for many diff. use cases from delivering web content, to archiving, to disaster recvovery planning
    * Use Cases: images, large media files, backups

#### NoSQL
* Cloud Datastore - provides a scalable, schemaless solution...use this when NoSQL is right for your app. This also supports ACID transactions and custom indexes for extremely fast reads. Good choice when building app engine applications.
    * Use Cases: user profiles, product catalog

* Cloud Bigtable - offers co.s a fast, fully managed, infinitely scalable DB service ideal for web/mobile/iOT apps. ideal when accumulating HUGE amounts of data at fast rates.
    * Use Cases: AdTech, Financial and IoT data

^ Bc of its ability to create custom indexes, Cloud DataStore is optimized for very fast queries. Indexing comes at a cost, though, and they make writes slower. Cloud Bigtable has no custom indexes and thus is optimized for writes...use bigtable when you need to accumulate a lot of data very quikcly

#### SQL
* Cloud SQL - gives you a fully managed MySQL.. use it when you want a relational DB in the cloud..this is great for existing apps and wb frameworks
    * Use Cases: user credentials, customer orders



### Lab
* we'll deploy another version of the bookshelf app, but add support for displaying the book covers using Cloud Storage...we'll create a Cloud Storage bucket, deploy the bookshelf app to App Engine using Cloud Shell, and then test in the browser and view the objects in Cloud Storage.

diagram: bookshelf is an app deployed using app engine. data about the books is stored in Google Cloud Datastore (Structured), the book images are jpgs stored inside Cloud Storage buckets (unstructured).

we're still using google cloud datastore for structured data but using Google Cloud Storage for images, using both storage services side by side. 

##### Cloud Storage Overview
when you store data in Google Cloud storage, you choose a storage class and location to find the right balance of availability, latency, and price for your data. after you choose the class and loacation, organize data in containers called BUCKETS. BUCKETS STORE THE DATA (BLOBS) YOU UPLOAD.

you can create and manage multiple buckets within a project. No buckets within buckets! The objects you put in these buckets are comprised of data and metadata. Metadata is used to control how the data is accessed (via GET requests). when you upload objects to Cloud Storage, the cannot be nested. 

##### steps
1. we'll create a bucket with the Google Cloud Shell:
`gsutil mb -l <location> gs://$DEVSHELL_PROJECT_ID` 
* `gsutil` = create a bucket
* `mb`= make a bucket
* `-l` = location
* `gs://` = bucket names begin wit this prefix

2. use the `defacl` command to give `AllUsers` read access to your bucket. Type the command to configure the default ACL (what is that?) for your new bucket to ensure all new objects are publicly accessible:

`gsutil defacl ch -u AllUsers:R gs://$DEVSHELL_PROJECT_ID`

3. check out source code for Bookshelf with the name of your new bucket in `cloud-storage/bookshelf/storage.py` in addition to code in `cloud-storage/bookshelf/templates/list.html` for seeing the `ImageUrl` conditional in the markup.


4. 
* return to Cloud Shell and add this command to change to a directory:  `cd ~/cp100/default/cloud-storage`
* type this command to replace the bucket name placeholder in `config.py`with the value of your project ID: `sed -i s/your-bucket-name/$DEVSHELL_PROJECT_ID/ config.py`...

^ 'your-bucket-name' refers to placeholder text already existing in `config.py`.


5. add the libraries the app uses: `pip install -r requirements.txt -t lib`
6. now deploy `gcloud app deploy`

7. go to the address, add a book, check it out in the view and also via `storage > storage > YOUR BUCKET`




# Google Container Engine
nowadays, its super common to deploy apps in containers. containers offer many advatnages: portability, scalability, etc. They gotta be managed though, and we'll see how google container engine can be used to make running containerized apps easier. 

# Introduction To Containers
containers are very lightweight VMs that can run on any machine, including other VMs. A single machine can run multiple containers at the same time (if it has enough resources). 

* containers be configured to run apps independently of the OS the containers run on.
* containers are basically Virtualization at the OS layer
* containers separate OS from app code and app dependencies
* most popular = Docker

* since containers are always the same, theres less likely to have an unexpected problem when moved from `dev to test to prod` environments.

### Why use containers instead of VMs?
* simple, consistent across dev, testing, and prod envs
* loose coupling b/w application and OS layers - by abstracting just the OS instead of the whole computer, it takes much less time for an app in a container to be ready to take requests than booting an entire VM...this makes autoscaling apps much more efficient.
* containers also allow devs to further sub-divide compute resources
* containers make apps really portable...go from one machine to another OR one cloud to another very easily!!


# Kubernetes
* deploying lots of containers does require a lot of mgmt though....we use Kubernetes to simplify this.

Kubernetes = open source container cluster system....huh?

its common to divide large apps into multiple micro-services and the micro-services need to scale up/down, there needs to be load bouncers directing traffic to the right container, someone has to monitor the health of the container and restart them if they go down. 

^ Kubernetes does this AND is based on google's experience running containerized apps over the years.
* is open-source

####  features
* portability across any system...run it on laptop, datacenter, and cloud envs
* supports rolling updates (zero downtime)
* monitors load and turns machines on/off depending on demand,. 
* it abstracts the implementation details of how storage is provided from how your app uses storage AKA 'kubernetes abstracts the details of how storage of how its provided from how its consumed'

* containers run on clusters of machines and these clusters can be rn in multiple zones for greater fault tolerance. Kubernetes takes care of load balancing and health monitoring for you. It routes traffic to an appropriate container instance.


^ This all sounds great but I need to see it.

# Google Container Engine
* This is the service that runs your containerized app on google's infrastructure. 

^ This is only one of several google cloud platform compute options for running your applications (like App Engine for example).

* container engine is between Compute Engine, which is infrastructure as a service, and also between App Engine, which is Platform as a service. Container engine allows for greater control for how infrastructure is created than App Engine.

* Kubernetes combined with Container Engine allow a more programmatic way of allocating resources rather than specifiying IP addresses, health checks, etc.

Google Container engine is:
    1. a fully manage cluster mgmt and orchestration system for running containers...you specify how big a cluster you want and where, and VMs are created automatically for you. Container Engine also uses Compute ENgine instances
    2. other compliementary services:
        * Google Container Registry - when you build containers you can upload and store them in container registry. This registry keeps the docker images in cloud storage and those images can be used to start your applications.

**Google Container Engine hides many of the details required to run your app. You use a declarative syntax to define how you want your apps to be deployed...container engine then cereate machines, deploys containers, monitors container health, and handles load balancing and auto-scaling.**

many co.s wanna do a more dev-ops approach to deploying apps. This means that Dev and IT are able to make use of similar tools and workflows...Kubernetes and its declaritive approach to infrastrucuter support this type of environemnt.



#### Compute Services
[[insert slide IaaS and PaaS HERE!!!]]

#### Deploying Apps: Container Engine vs. App Engine
Container Engine supports and programming language and has a hybrid service model. Its primary use case is container-based workloads. You get many of the advantages of PaaS offerings like App ENgine, including automated monitoring, sclaing, and load balancing

App Engine supports various languages, and its primary model is a PaaS, primarily used for web/mobile apps. 

App Engine Flexible also supports use cases 


# Lab
what I'll learn: how to create a container using the Cloud SDK, build and push a bookshelf 'image' to Container Registry, and use Kubectl to deploy the Bookshelf container.

Kubernetes Engine is only used in this lab to host the frontend Python web component of the app. you contine to use the exisint GCP Storage services used earlier.

Again: Kubernetes is a cluster manager and orchestrator for running Docker containers. Kubernetes Engine schedules containers into a cluster and automatically manages them based on requirements you define DECLARITIVELY using config files. 

* Kubernetes Engine clusters run on Compute Engine instances or VMs known as nodes. In this lab you'll creat ethe container cluster using the Cloud SDK and then use the kubectl command line tool to deploy and scale containers running Bookshelf on Kubernetes. 

* 1 major advantage of this version of Bookshelf is that its straightforward to deploy the code to any env running the required version of kubernetes...your laptop, on premises, or even hybrid deployments across public and private clouds. 

Kubernetes includes a number of concepts that are new to me:

    1. Nodes - a worker machine in a Kubernetes cluster, and in Google Kubernetes Engine, the machine is always a 'Compute Machine Instance.' In this lab you'll create a cluster with 2 Compute Engine Nodes.

    2. Pods - a group of one or more containers, shared storage, and config data relating to those containers. Its common for production apps running in Kubernetes to include multiple, relatively tightly coupled containerrs in a single pod. For the purpose of our Demo, a single Bookshelf Docker container runs inside each pod.

    3. Replication Controllers - this works to ensure that the requested number of pod replicas are always avaialbe and running at a given time. It automatically adds/removes pods as required to maintain a desire state. in this lab, you specify that the replication controller should create 3 replicas, running across your 2 nodes. One you deploy the replication controller...there will be 3 identifcal pods running the frontend component of the Bookshelf app.

    4. Services - this defines a logical set of pods and a wat to access them using an IP address and port number pair. 


    * In addition to Kubernetes Engine, you also use Google Container Registry to host a Docker image of Bookshelf. Container Registry provides a secure,private Docker image storage on Google Cloud Platform. 

    [[ INSERT GOOGLE CLOUD PLATFORM PROJECT DIAGRAM SCREENSHOT HERE!!! ]]

    ## Steps

* This lab was THE WORST!!!!!

# Google Compute Engine
* this is googles IaaS offering...it allows you to run Viurtial linux and windows machines in the cloud. The networking services allow you to build secure, worldwide network environemnets and configure load balancers, routes, firewalls and more.

## Overview
- its another one of GCP's 'Compute' options, like App Engine or Container Engine (that we just used)
- this lets you create and run VMs on googles' infrastructure
- an instance is a VM hosted on google's infrastructure...you can use this via Gcloud CLI or your own command line.
- in creating an instance you can choose: how many CPUs, how much RAM, and how much disk space should be allocated to that machine. more resources = more $

- theres also privacy controls when relating to 

- pay-per-minute billing. 


## Google Cloud Networking overview
* plenty of diff. options to choose from:
    carrier interconnect - enterprise-grade connections provided by carrier service providers
    direct peering - connect business directly to google
    CDN interconnect - allows CDN providers to establish direct interconnect links with google's edge network at various locations.

    ^ def. not understanding this. 


Google Cloud VPN - connects your network to Google Cloud Platform
* encrypts traffic over the internet

Google Cloud DNS - highly available and scalable....you can create managed zones, then add/edit/delete DNS records.

Google Cloud Load Balancing - allows you to distrubute traffic across multiple machine instances. Load Balancer can also detect machine health and take unhealthy machines out of service.

* If you ciombone auto-scaling rules and a load balancer, its easy to create elastic systems that turn machines on/off depending on demand.

theres a couple diff load balancers with GCP
* HTTP load balancer - servers can be located in multiple regions around the world...its even smart enough to dwetect where user is and direct them to a service in the closest location. you can also enable google's worldwide CDN for greater performance, reduced cost. 
^ only works with http/https protocols.

Cant work with http/https? use TCP/UDP load balancer

* TCP/UDP load balancer


* Google Cloud CDN 
 - leverages google's globally distributed web caches served out of Google Compute Engine.
 - Cloud CDN uses caches at network locations to store responses generated by instances. 

 ## Operations and Tools
 * a number of operational and tooling services to support your apps...we'll chat about those:

 * Google Stackdriver - integrated monitoring, logging, diagnostics, works across GCP, AWS as well. What this does:
    * Monitoring - monitors usage, uptime and health....you can set up dashboards and alerts as well. 
    * Logging - every request in GCP is logged, your app can also write to logs...these logs can be searched, filtered by service, and analyzed to gain insight into customer use and usage...
    * Trace - trace utility can show you how long requests take and show what happened within each request. This can be used when trying to track down a performance problem.
    * Error Reporting - you know
    * Debugger - Helps with tracing!


* Google Cloud Deployment Manager
    * Infrastructure mgmt service...
    * create a .yaml template describing your env and use deployment manager to create resources.
    * this provides repeatable deployments.

* Google Cloud Repositories
    * provides git repos for your projects. 
    * includes a source code editor and integrates with cloud debugger for setting breakpoints and finding bugs in source code. 

* Google Cloud Functions
    * create single-purpose functions that respond to events without a server or runtime
        Event examples: New instance created, file added to storage
    * written in JS, execute in managed Node.js env on GCP
    * events from GC storage and pubsub can trigger cloud fns, OR you can use HTTP invocation for synchronous execution.

    * cloud events are just things that happen in your cloud environment....this could be something like:
        * changes in data in a DB
        * files added to storage
        * new VM instance created

    events occur whether you choose or not to respond to them...creating a response is through a 'trigger'. trigger = a declaration that you're interested in an event or a sequence of events.
**^ Serverless**


## Lab
* review: 
    Compute Engine lets you create and run VMs (known as instances), block storage disk devices, and networks on Google infrastructure. Compute Engine instances are created based on a combo of boot image and machine type.

    No upfront investment is required to get started and you can run 1000s of virtual CPUs on infrastructre thats designed to be fast and offer strong, consistent performance. 

    Compute Engine instances and resources are used as the basis of many other service in GCP. 

    EX: Container Engine leverages Compute Engines instances. 




    Questions I actually have:

    - why use Compute Engine over App Engine? 
    - What am I actually gonna use? 

    - is this all literally just for creating and deploying instances??
    - looks like how to use the GUI and then how to operate stuff via either your terminal or the gcloud shell....anything else I'm missing here?





# Google's Big Data platform

## Big Data 
Offerings
1. BigQuery - analytics Data warehouse
    * analyze huge datasets in seconds allowing you real-time stuffs
    * you can use SQL-like syntax so fairly easy to pick up
    * basically unlimited storage

    EX: Shine company...here they were able to analyze 2 billion of rows of data in 20-25secs..

2. Cloud Pub/Sub - scalable & flexible enterprise messaging
    * can handle a million messages/second
    * includes offline support

    Use cases: collect data from IoT devices/sensors, used to implement push notifications in cloud apps, etc


3. Cloud Dataflow -stream & batch processing
    * used for creating data processing pipelines...
        Use cases: 
            * used for batch extract/transform/load pipeline jobs to either move, filter, enrich, or shape data
            * data analysis - batch computation or continuous computation using streaming.
            * Orchestration - you can create pipelines that coordinate services, including external services. 
4.  Cloud Dataproc - 

5. Cloud Datalab - its an interactive tool for large-scale data exploration, transformation, analysis, and visualization.
    * allows Data Scientists to collaborate on big data analysis and machine learning models.
    * Combines code, docs, results, and visualizations in intuitive notebook format. 

**^ fully managed, no-ops services**

## Machine Learning 

Services:
* Cloud Machine Learning
* Vision API - analyze images with a simple REST api: face detection, logo detection, label detection, etc. you can gain insight from images, detect inappropriate content, analyze sentiment. Images can be analyzed when uploaded or with images already in Google Cloud Storage.

* Speech API - can return text in real-time...you can command control via voice.

* Translate API

google offers a complete ML Platform used to build ML models, use them, and integrate them in your apps. Tensorflow is the framework they use...its open source. Its used to make ML models easier...to use this on GCP, you can use Cloud ML. Cloud ML a fully managed no-ops sevice for running tensorflow models. it provides faster execution and more accurate training for the models you create.

Google also offers a series of pre-trained learning models:
* vision: identify objects, landmarks, text, explicit content detection
* translate: includes language detection
* speech: stream results in real-time, detects 80 languages
